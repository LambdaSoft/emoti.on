emoti.on
========

“We transform your love into sentences.”

Give it a try: http://emotion.cyberlove.us

emoti.on 0.2c
-----------
[+] Layout fix for Chrome, Firefox and IE10.

[+] New fonts.

emoti.on 0.2b
-----------
[+] Now the “submit” button won't show anymore when you click it.

[+] Added a check for empty strings on custom mode.

[+] Now the logo in api.html has the cute heart too.


emoti.on 0.2a [aka “Coffee Edition”]
-----------

[+] New “submit” button

[+] New sentences


[+] Added the custom mode

[+] Added the custom mode (powered by coffee... SO MUCH COFFEE)


[+] Added “custom” button

[+] New background in api.html

[+] New animations

[+] New alphabet for encryption

[+] New font and color in api.html

[+] Replaced “+“ to “\” for the split between the two names


emoti.on 0.1b
-----------

[+] Some CSS fixes for other browsers

[+] Added a link to “index.html” in api.html

[+] Added a cute heart on the main logo


emoti.on 0.1a
-----------

[+] First release

[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/JustHvost/emoti.on/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

