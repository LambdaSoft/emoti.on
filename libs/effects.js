function logo_animation() { $('h1').animo({animation: "bounce", duration: 1}); }

function start_default() { 
	$('#start_custom').animo({animation: 'flipOutX', duration: 0.5});
	$('#start_default').animo({animation: "bounceOut", duration: 0.5}, function(){
		$('#start_custom').hide();
		$('#start_default').hide();
		$('section#default input#name, section#default input#name2').show().animo({animation: 'bounceInLeft', duration: 0.5});
		$('#submit_default').show().animo({animation: 'bounceIn', duration: 0.1});
	}); 
}


function start_custom() { 
	$('#start_custom').animo({animation: 'bounceOut', duration: 0.5});
	$('#start_default').animo({animation: "flipOutX", duration: 0.5}, function(){
		$('#start_custom').hide();
		$('#start_default').hide();
		$('section#custom input#name3, section#custom input#content').show().animo({animation: 'bounceInLeft', duration: 0.5});
		$('#submit_custom').show().animo({animation: 'bounceIn', duration: 0.1});
	}); 
}

function got_love_here(number) {
	switch(number) {
		case '1':
			$('#submit_default').animo({animation: 'bounceOut', duration: 0.5, keep: true}, function(){ $('#submit_default').hide(); });
			$('input#name, input#name2').animo({animation: 'bounceOutLeft', duration: 0.5, keep:true}, function(){
				$('input#link').show().animo({animation: 'tada', duration: 1});
				$('p#about').show().animo({animation: 'bounceInRight', duration: 0.5});
			});
		break;
		case '2':
		    $('#submit_custom').animo({animation: 'bounceOut', duration: 0.5, keep: true}, function(){ $('#submit_custom').hide(); });
			$('input#name3, input#content').animo({animation: 'bounceOutLeft', duration: 0.5, keep:true}, function(){
				$('input#link').show().animo({animation: 'tada', duration: 1});
				$('p#about').show().animo({animation: 'bounceInRight', duration: 0.5});
			});
		break;
	}
}

function submit_animation(number) { 
	switch(number) {
		case '1':
			$("#submit_default").animo({animation: 'wobble', duration: 0.5});
			break;
		case '2':
			$("#submit_custom").animo({animation: 'wobble', duration: 0.5}); 
			break;
	}
}